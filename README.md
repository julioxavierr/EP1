Para executar o programa, entre na pasta bin e execute arquivo finalBinary.keep:
$cd ./bin/
$./finalBinary.keep

Este automaticamente criará na pasta principal do programa:
copia.pgm - copia sem modificações
negativo.pgm - imagem com filtro negativo aplicado
smooth.pgm - imagem com filtro smooth aplicado
sharpen.pgm - imagem com filtro sharpen aplicado



1 - Para remover os arquivos .o e o binario
	$make clean

2 - Para compilar o código
	$make
	$cd bin/
	$./finalBinary

OBS.: A imagem .pgm tem que estar, necessariamente, na pasta raiz do projeto

