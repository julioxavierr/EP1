#include "../include/imagem.hpp"
#include "../include/filtro.hpp"
#include "../include/negativo.hpp"
#include "../include/smooth.hpp"
#include "../include/sharpen.hpp"

using namespace std;

int main(int argc, char *argv[]){
	Imagem first(512, 512, 255);	//criado um objeto do tipo Imagem

	first.lerImagem("../lena.pgm");
	first.salvarImagem("../doc/copia.pgm");	//fazendo um teste
	/*Somente esta cópia foi salva na pasta doc, como é pedido no tópico 2.3 do pdf. As outras foram salvas no mesmo local da original, para facilitar a correção*/


	//Aplicando filtro negativo
	Negativo filtroNegativo;	//objeto do tipo negativo
	filtroNegativo.aplicarFiltro(first);
	first.salvarImagem("../negativo.pgm");



	Imagem second(512, 512, 255);	//criando novo objeto do tipo Imagem
	second.lerImagem("../lena.pgm");
	//Aplicando filtro smooth
	Smooth filtroSmooth;
	filtroSmooth.aplicarFiltro(second);
	second.salvarImagem("../smooth.pgm");

	Imagem third(512, 512, 255);
	third.lerImagem("../lena.pgm");
	//Aplicando filtro sharpen
	Sharpen filtroSharpen;
	filtroSharpen.aplicarFiltro(third);
	third.salvarImagem("../sharpen.pgm");

	return 0;
}
