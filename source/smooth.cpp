#include "../include/smooth.hpp"
#include <iostream>
#include <stdlib.h> //para poder usar o div
#include <vector>

using namespace std;

Smooth::Smooth(){
	setDiv(9);
	setSize(3);
}

void Smooth::aplicarFiltro(Imagem &imagem){
	int i, j, value, x, y;
	int filtro[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};

	int tamanho = getSize();

	//vetor auxiliar
	int *aux = new int[imagem.getNumDeLinhas() * imagem.getNumDeColunas()];

	for(i=tamanho/2; i<imagem.getNumDeLinhas()-tamanho/2; i++)
    {
        for(j=tamanho/2; j<imagem.getNumDeColunas()-tamanho/2; j++) 
        {
        	
        	value = 0;
        	
        	for(x = -1; x <=1; x++){
        		for(y = -1; y<=1; y++){
        			value += filtro[(x+1)+ tamanho*(y+1)]*imagem.getPixel((i+x), (y+j));
        		}
        	}

        	value /=getDiv();

        	value = value < 0 ? 0 : value;
			value = value > 255 ? 255 : value;

			aux[i+imagem.getNumDeColunas()*j] = value;
        }
    }


	for(i=0; i<imagem.getNumDeLinhas(); i++)
    {
        for(j=0; j<imagem.getNumDeColunas(); j++) 
        {
        	imagem.modificaPixel(i, j, aux[i+imagem.getNumDeColunas()*j]);
        }
    }

	//Abaixo a forma que eu aplicava o filtro anteriormente 
	/*for(i=1; i<imagem.getNumDeLinhas()-1; i++)
    {
        for(j=1; j<imagem.getNumDeColunas()-1; j++) 
        {
        	
        	value = 0;

			//Nas linhas seguintes, de acordo com os valores que estão ao redor do
			//pixel/elemento na posição i, j da matriz é feita uma média

        	value = (imagem.getPixel((i+1), j) + imagem.getPixel((i+1), (j+1)) + imagem.getPixel((i+1), (j-1)) + imagem.getPixel((i-1), j) + imagem.getPixel((i-1), (j+1)) + imagem.getPixel((i-1), (j-1)) + imagem.getPixel(i, (j+1)) + imagem.getPixel(i, (j-1)))/8;


        	value = value < 0 ? 0 : value;
        	value = value > 255 ? 255 : value;

			imagem.modificaPixel(i, j, (int)value);
        }
    }*/


    cout << "Filtro smooth aplicado" << endl;
}
