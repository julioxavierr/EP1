#include "../include/imagem.hpp"

#include <iostream>
#include <string>
#include <fstream>	//biblioteca stream para manipular arquivos
#include <sstream>	//biblioteca stream para operar strings
#include <stdlib.h> 

using namespace std;

Imagem::Imagem() {
	numDeLinhas = 0;
	numDeColunas = 0;
	maxDeCinza = 0;

	matriz = new int[numDeLinhas*numDeColunas];
}

Imagem::Imagem(int numDeLinhas, int numDeColunas, int maxDeCinza) {
	//o this elucida que estamos apontando para um atributo de instância da classe
	this->numDeLinhas = numDeLinhas;
	this->numDeColunas = numDeColunas;
	this->maxDeCinza = maxDeCinza;
	matriz = new int[numDeLinhas*numDeColunas];
}


/* matriz[i][j] foi reescrito como
	matriz[i*sizeY+j]*/

Imagem::~Imagem(){
	//desaloca a matriz;
	delete []matriz;
}

int Imagem::getNumDeLinhas() {
	return numDeLinhas;	
}

int Imagem::getNumDeColunas() {
	return numDeColunas;
}

int Imagem::getMaxDeCinza() {
	return maxDeCinza;
}

void Imagem::lerImagem(const char fname[]) {
	int i, j;
    unsigned char *charImage;
    char header [100], *ptr;
    ifstream ifp;

    ifp.open(fname, ios::in | ios::binary);

    if (!ifp) 
    {
        cout << "Nao e possivel ler a imagem: " << fname << endl;
        exit(1);
    }

 // le o cabeçalho

    ifp.getline(header,100,'\n');
    if ( (header[0]!=80) || (header[1]!=53) ) // P5
    {   
        cout << "Imagem " << fname << " nao e PGM" << endl;
        exit(1);
    }

    ifp.getline(header,100,'\n');
    while(header[0]=='#')
        ifp.getline(header,100,'\n');

    numDeLinhas=strtol(header,&ptr,0);
    numDeColunas=atoi(ptr);

    ifp.getline(header,100,'\n');
    maxDeCinza=strtol(header,&ptr,0);

    charImage = (unsigned char *) new unsigned char [numDeLinhas*numDeColunas];

    ifp.read( reinterpret_cast<char *>(charImage), (numDeLinhas*numDeColunas)*sizeof(unsigned char));

    if (ifp.fail()) 
    {
        cout << "Imagem " << fname << " esta com o tamanho errado" << endl;
        exit(1);
    }

    ifp.close();

 // Converte unsigned char para int
 

    int val;

    for(i=0; i<numDeLinhas; i++)
        for(j=0; j<numDeColunas; j++) 
        {
            val = (int)charImage[i*numDeColunas+j];
            matriz[i*numDeColunas+j] = val;   
        }

	//desalocando charImage
    delete [] charImage;
}

void Imagem::salvarImagem(const char filename[]) {
	int i, j;
	unsigned char *charImage;
	ofstream outfile(filename);
	
	charImage = (unsigned char *) new unsigned char [numDeLinhas*numDeColunas];
	
	int val;
	
    for(i=0; i<numDeLinhas; i++)
    {
        for(j=0; j<numDeColunas; j++) 
        {
			// matriz[i][j] foi reescrito como
			//matriz[i*sizeY+j]
			val = matriz[i*numDeColunas+j];
			charImage[i*numDeColunas+j]=(unsigned char)val;
        }
    }
    
	if (!outfile.is_open())
	{
		cout << "Nao e possivel abrir o arquivo de saida"  << filename << endl;
		exit(1);
	}
	
	outfile << "P5" << endl;
    outfile << numDeLinhas << " " << numDeColunas << endl;
    outfile << 255 << endl;
	
	//pixels
	outfile.write(reinterpret_cast<char *>(charImage), (numDeLinhas*numDeColunas)*sizeof(unsigned char));
	
	cout << "Arquivo " << filename << " criado com sucesso!" << endl;
	outfile.close();
}

void Imagem::modificaPixel(int i, int j, int novoValor){
    matriz[i*numDeColunas+j] = novoValor;
}

int Imagem::getPixel(int i, int j){
    return matriz[i*numDeColunas + j];
}
