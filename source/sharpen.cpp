#include "../include/sharpen.hpp"
#include <iostream>

using namespace std;

Sharpen::Sharpen(){
    setDiv(1);
    setSize(3);
}

void Sharpen::aplicarFiltro(Imagem &imagem){
	int i, j, value, x, y;
	int filtro[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};

    int tamanho = getSize();

	//vetor auxiliar
	int *aux = new int[imagem.getNumDeLinhas() * imagem.getNumDeColunas()];

	for(i=tamanho/2; i<imagem.getNumDeLinhas()-tamanho/2; i++)
    {
        for(j=tamanho/2; j<imagem.getNumDeColunas()-tamanho/2; j++) 
        {
        	
        	value = 0;
        	
        	for(x = -1; x <=1; x++){
        		for(y = -1; y<=1; y++){
        			value += filtro[(x+1)+ tamanho*(y+1)]*imagem.getPixel((i+x), (y+j));
        		}
        	}

        	value /=getDiv();

        	value = value < 0 ? 0 : value;
			value = value > 255 ? 255 : value;

			aux[i+imagem.getNumDeColunas()*j] = value;
        }
    }


	for(i=0; i<imagem.getNumDeLinhas(); i++)
    {
        for(j=0; j<imagem.getNumDeColunas(); j++) 
        {
        	imagem.modificaPixel(i, j, aux[i+imagem.getNumDeColunas()*j]);
        }
    }

    cout << "Filtro sharpen aplicado" << endl;
}
