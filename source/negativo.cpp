#include "../include/negativo.hpp"
#include "../include/imagem.hpp"
#include <iostream>

using namespace std;

Negativo::Negativo(){

}

void Negativo::aplicarFiltro(Imagem &imagem){
	int i, j;
	int tom = imagem.getMaxDeCinza();
	for(i=0; i<imagem.getNumDeLinhas(); i++)
    {
        for(j=0; j<imagem.getNumDeColunas(); j++) 
        {
			//"revertendo" as tonalidades de cinza da matriz
        	int novoValor = tom - imagem.getPixel(i, j);
			imagem.modificaPixel(i, j, novoValor);
        }
    }

    cout << "Filtro negativo aplicado!" << endl;
}
