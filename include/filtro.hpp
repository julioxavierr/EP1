#ifndef FILTRO_H
#define FILTRO_H

#include "imagem.hpp"

//Classe Filtro, superclasse ou classe pai das classes Negativo, Smooth, Sharpen
class Filtro{
private:
	int filter[];	//Atributo privado da classe pai
	int div;
	int size;
public:
	Filtro();

	//Métodos construtores
	void setDiv(int div);	
	void setSize(int size);	

	//Métodos acessadores
	int getDiv();		
	int getSize();			

	//O uso de virtual permite que nas subclasses esse método possa ser redefinido
	virtual void aplicarFiltro(Imagem &imagem){
		return;	
		//estava dando o seguinte erro - filtro.cpp:(.text+0xf): referência indefinida para `vtable for Filtro'
	}

};

#endif
