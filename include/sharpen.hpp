#ifndef SHARPEN_H
#define SHARPEN_H
#include "filtro.hpp"

//Classe Sharpen que herda de Filtro
class Sharpen : public Filtro{
public:
	Sharpen();

	void aplicarFiltro(Imagem &imagem);
};

#endif
