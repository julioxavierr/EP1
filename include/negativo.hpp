#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"	//para herdar, é necessário importar o header da classe pai

//Classe Negativo que herda da Classe Filtro, portanto é uma Subclasse de Filtro
class Negativo : public Filtro{
public:
	Negativo();

	void aplicarFiltro(Imagem &imagem);

};

#endif
