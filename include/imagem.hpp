#ifndef _IMAGEM_H_		//se não estiver definido
#define _IMAGEM_H_		//defina

//não necessita de importar bibliotecas

using namespace std;

//Classe
class Imagem
{
	private:
		//Estes são os atributos da classe Imagem, estes são privados
		int numDeLinhas;
		int numDeColunas;
		int maxDeCinza;
		int *matriz;	//será alocada junto com a criação do objeto

	public:
		//Construtores
		Imagem();
		Imagem(int numDeLinhas, int numDeColunas, int maxDeCinza);

		//Destrutor
		~Imagem();

		//Getters (métodos de carregamento)
		int getNumDeLinhas();
		int getNumDeColunas();
		int getMaxDeCinza();

		//Métodos de manipulação de imagem, os nomes são auto-explicativos
		void lerImagem(const char fname[]);
		void salvarImagem(const char filename[]);

		/*Método para obter o valor (getter) do elemento na posição i, j
		da matriz de pixels*/
		int getPixel(int i, int j);
		//Método para instanciar o valor de determinada casa da matriz de pixels
		void modificaPixel(int i, int j, int novoValor);
		
};

#endif	//fim da definição
