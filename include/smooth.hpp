#ifndef SMOOTH_H
#define SMOOTH_H
#include "filtro.hpp"

//Classe Smooth que herda de Filtro
class Smooth : public Filtro{
public:
	Smooth();

	void aplicarFiltro(Imagem &imagem);

};

#endif
